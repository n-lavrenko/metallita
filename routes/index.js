const keystone = require('keystone');
const middleware = require('./middleware');
const importRoutes = keystone.importer(__dirname);
const cookieParser = require('cookie-parser');
const i18n = require('i18n');
const enquiryApi = require('../api/enquiry'); 
const requestApi = require('../api/request'); 


keystone.pre('routes', middleware.initLocals); // local variables for view (.pug)

// Import Route Controllers
let routes = {
	views: importRoutes('./views')
};


exports = module.exports = function (app) {
	app.use(cookieParser());

	app.get('/setlocale/:locale', function (req, res) {
		res.cookie('locale', i18n.setLocale(req.params.locale));
		res.redirect('back');
	});
	
	app.get('/', routes.views.index);
	
	app.get('/contacts', routes.views.contacts);
	app.post('/api/contacts', enquiryApi.post);
	
	app.post('/api/request', requestApi.post);
	
	app.get('/products', routes.views.products);

};
