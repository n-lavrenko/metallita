let keystone = require('keystone');

exports = module.exports = function (req, res) {
	let view = new keystone.View(req, res);
	let locals = res.locals;

	locals.section = 'products';

	view.query('productList', keystone.list('ProductList').model.findOne().where('lang', res.locals.currentLanguage).populate('products'));
	
	view.render('products');
};
