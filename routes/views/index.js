const keystone = require('keystone');

exports = module.exports = function (req, res) {

	let view = new keystone.View(req, res);
	let locals = res.locals;

	locals.section = 'home';
	
	view.query('profit', keystone.list('ProfitList').model.findOne().where('lang', res.locals.currentLanguage).populate('profits'));
	view.query('serviceList', keystone.list('ServiceList').model.findOne().where('lang', res.locals.currentLanguage).populate('services'));
	view.query('stageList', keystone.list('StageList').model.findOne().where('lang', res.locals.currentLanguage).populate('stages'));
	
	view.render('index');
};
