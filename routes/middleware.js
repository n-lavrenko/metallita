const keystone = require('keystone');
const i18n = require('i18n');

/**
	Initialises the standard view locals
*/
exports.initLocals = function (req, res, next) {
	res.locals.navLinks = [
		{ key: 'home', href: '/' },
		{ key: 'products', href: '/products' },
		{ key: 'contacts', href: '/contacts' },
	];
	res.locals.user = req.user;
	
	res.locals.languages = i18n.getLocales();
	
	let currentLanguage = i18n.getLocale();
	res.locals.currentLanguage = currentLanguage;
	
	res.locals.i18n = i18n;
	
	let queries = [];
	queries.push(keystone.list('Info').model.findOne().where('lang', currentLanguage).exec());
	queries.push(keystone.list('Contact').model.findOne().where('lang', currentLanguage).exec());

	Promise.all(queries).then(function (docs, err) {
		if (err) {
			throw err;
		}
		res.locals.info = docs[0]._doc;
		res.locals.contacts = docs[1]._doc;
		
		next();
	});
	
};

/**
	Prevents people from accessing protected pages when they're not signed in
 */
exports.requireUser = function (req, res, next) {
	if (!req.user) {
		res.redirect('/keystone/signin');
	} else {
		next();
	}
};
