let keystone = require('keystone');
let Enquiry = keystone.list('Enquiry');

module.exports.post = (req, res) => {
	let data = req.body;
	
	let enq = new Enquiry.model({
		name: data.name,
		email: data.email,
		phone: data.phone,
		message: data.message,
	});
	
	enq.save(function (err) {
		if (err) {
		  console.log('err -- ', err);
			next(err);
		}
		res.status(200).json({
			message: 'Created'
		});
		
	});
	
};
