let moment = require('moment');
let mailgun = require('mailgun-js')({
	apiKey: process.env.MAILGUN_API_KEY,
	domain: process.env.MAILGUN_DOMAIN
});

module.exports.post = (req, res) => {
	let data = req.body;
	
	let formData = {
		name: data.name,
		phone: data.phone,
		email: data.email,
		rows: JSON.parse(data.rows),
		createdAt: moment().format('DD.MM.YYYY HH:mm')
	};

	sendEmail(function (err, body) {
		if (err) {
			return res.status(500).json({
				success: false,
				message: 'Error with email sending',
				error: err
			});
		}

		res.status(200).json({
			success: true,
			message: 'Request was sended'
		});
	});

	function sendEmail(callback) {
		let rowsTr, table;
		if (formData.rows) {
			rowsTr = formData.rows.map(row => `
				<tr>
					<td>${row.name || ''}</td>
					<td>${row.code || ''}</td>
					<td>${row.count || ''}</td>
					<td>${row.wide || ''}</td>
					<td>${row.note || ''}</td>
				</tr>`);
			table = `
				<table class="table">
					<thead>
						<td>Наименование</td>
						<td>Код Изделия</td>
						<td>Количество</td>
						<td>Толщина мм</td>
						<td>Примечание</td>
					</thead>
					<tbody>
						${rowsTr.join('')}
					</tbody>
				</table>`
		}
		
		let template = `
		<html>
			<head></head>
		<body>
			<style>
			table {border-collapse: collapse;}
			table td {border: 1px solid; padding: 7px;}
			table thead td { 
				font-weight: bold;
			}
			</style>
			<h2>Получена новая заявка с Metallita.com</h2>
			<p><strong>Имя: </strong>${formData.name}</p>
			<p><strong>Телефон: </strong> ${formData.phone}</p>
			<p><strong>Email: </strong>${formData.email}</p>
			<p><strong>Когда: </strong>${formData.createdAt}</p>
			<hr>
			<p>
				<strong>Прикрепленый файл: </strong>${req.files.file ? req.files.file.originalname: ' Отсутствует'}
			</p>
				${table}
		</body>
		</html>`;

		let emailData = {
			from: 'metallita.com' + '<info@metallita.com>',
			to: 'laser.metallita@gmail.com',
			subject: 'Заявка с metallita.com',
			html: template
		};
		
		if (req.files.file) emailData.attachment = req.files.file.path;

		mailgun.messages().send(emailData, function (error, body) {
			callback(error, body);
		});

	}

};
