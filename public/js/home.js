$(function () {
	let brandSlider = $('.brand-name, .divider');
	let textSlider = $('.slider-text');
	let slider = $('.slider');

	slider.slick({
		autoplay: true,
		autoplaySpeed: 5000,
		pauseOnHover: false,
		pauseOnFocus: false,
		fade: true,
		speed: 500,
		cssEase: 'linear'
	})
		.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			$('.slider-text-' + currentSlide).textillate('stop');
			$('.slider-text-' + nextSlide).textillate('in');
			brandSlider.textillate('stop');
			brandSlider.textillate('in');
		});

	let mainAnimateOptions = {
		in: {effect: 'rotateInDownLeft'},
		autoStart: false
	};

	let brandAnimateOptions = {
		in: {
			effect: 'flash',
			shuffle: true
		},
		autoStart: false
	};

	textSlider.textillate(mainAnimateOptions)
		.on('outAnimationEnd.tlt', function () {
			$(this).textillate('stop');
			brandSlider.textillate('stop');
		});
	$('.slider-text-0').textillate('in');

	brandSlider.textillate(brandAnimateOptions);
	brandSlider.textillate('in');
	
	
	// Stages
	$('#stages').slick({
		infinite: true,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		// variableWidth: true,
		customPaging : function(slider, i) {
			return '<div class="dot-number">' + (i+=1) + '</div>';
		}
	});

	// countUp

	$(window).scroll(watchScroll);

	function watchScroll() {
		var targetEl = $('#counters-block');
		var hT = targetEl.offset().top,
			hH = targetEl.outerHeight(),
			wH = $(window).height(),
			wS = $(this).scrollTop();
		if (wS + targetEl.height() > (hT + hH - wH)) {
			startCounter();
			$(window).off('scroll', watchScroll);
		}
	}

	function startCounter() {
		$('.counter').each(function () {
			new CountUp($(this)[0], 0, +$(this).attr('data-count'), 0, 5, {useGrouping: false}).start();
		});
	}

});
