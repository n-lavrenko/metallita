(function () {
	angular.module('app', [])
		.controller('AppCtrl', AppCtrl);
	
	function AppCtrl($http) {
		var vm = this;
		
		vm.submit = submit;
		
		function submit() {
			if (vm.form.$valid) {
				
				$http.post('/api/contacts', vm.model, {headers: 'application/x-www-form-urlencoded'})
					.then(function () {
						vm.isSuccess = true;
					})
					.catch(function (err) {
						console.log(err);
					});
			} 
			else {
				vm.form.$setSubmitted(true);
			}
		}
	}
})();
