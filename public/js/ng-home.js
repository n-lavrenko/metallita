(function () {
	angular.module('app', ['ui.bootstrap.modal', 'uib/template/modal/window.html'])
		.controller('AppCtrl', AppCtrl)
		.controller('ModalCtrl', ModalCtrl)
		.directive('inputFile', function ($compile) {
			return {
				restrict: 'A',
				require: 'ngModel',
				scope: {
					ngModel: '='
				},
				link: function (scope, element, attr, ngModelCtrl) {
					var btn = angular.element('<button class="btn btn-upload" type="button">Выбрать файл</button>');
					var inputFile = angular.element('<input type="file" ng-model="' + attr.ngModel + '" onload="onloadHandler"/>');

					scope.onloadHandler = function(event, reader, file, fileList, fileObjs, object) {
						file.base64 = object;
					};

					element.attr('readonly', true);
					element.parent().append(inputFile).append(btn);
					$compile(inputFile)(scope);

					btn.bind('click', function () {
						inputFile[0].click();
					});

					inputFile.bind('change', function () {
						scope.$apply(function () {
							ngModelCtrl.$setViewValue(inputFile[0].files[0] );
						});
					});
				}
			}
		});

	function AppCtrl($uibModal) {
		var vm = this;
		vm.openPopup = openPopup;

		function openPopup() {
			$uibModal.open({
				templateUrl: 'js/modal.html',
				animation: true,
				size: 'lg',
				controller: 'ModalCtrl',
				controllerAs: 'm'
			});
		}
	}

	function ModalCtrl($http, $scope) {
		var m = this;

		var row = {
			name: null,
			code: null,
			count: null,
			wide: null,
			note: null,
		};
		
		m.model = {};
		m.model.rows = [];
		
		m.isError = false;
		m.isSuccess = false;
		m.sendText = {
			send: 'Отправить',
			sending: 'Отправляем'
		};
		
		m.submit = submit;
		m.close = close;
		m.addRow = addRow;
		
		
		function close($event) {
			if (!$event) return;
			$event.stopPropagation();
			$event.preventDefault();
			$scope.$close();	
		}
		
		function submit() {
			if (m.form.$valid) {
				m.isError = false;
				m.isSuccess = false;
				
				m.loading = true;
				
				$http({
					method: 'POST',
					url: '/api/request',
					headers: {'Content-Type': undefined},
					data: {
						name: m.model.name,
						phone: m.model.phone,
						email: m.model.email,
						rows: JSON.stringify(m.model.rows),
						file: m.model.file
					},
					transformRequest: function (data) {
						var formData = new FormData();
						angular.forEach(data, function (value, key) {
							formData.append(key, value);
						});
						return formData;
					}
				})
					.then(function () {
						m.isSuccess = true;
					})
					.catch(function (err) {
						m.isError = true;
						console.log(err);
					})
					.finally(function () {
						m.loading = false;
					});
			}
			else {
				m.form.$setSubmitted(true);
			}
		}
		
		addRow();
		
		function addRow() {
			m.model.rows.push(angular.copy(row));
		}
	}
	
})();
