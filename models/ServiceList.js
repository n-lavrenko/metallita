const keystone = require('keystone');
const Types = keystone.Field.Types;


let ServiceList = new keystone.List('ServiceList', {
	autokey: {
		from: 'name',
		path: 'key'
	}
});

ServiceList.add({
	lang: {type: Types.Select, options: [
		{ value: 'ru', label: 'Русский' },
		{ value: 'en', label: 'English' },
	],
		default: 'ru',
		initial: true,
		label: 'Язык', index: true
	},
	name: {type: String},
	description: {type: Types.Textarea, label: 'Описание' },
	services: {type: Types.Relationship, ref: 'Service', path: 'services', many: true, label: 'Сервисы'},

	image: {
		type: Types.CloudinaryImage,
		autoCleanup: true,
		select : true,
		label: 'Картинка блока'
	}
});


ServiceList.defaultColumns = 'name, lang';

ServiceList.register();
