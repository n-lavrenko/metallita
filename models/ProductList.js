const keystone = require('keystone');
const Types = keystone.Field.Types;


let ProductList = new keystone.List('ProductList', {
	autokey: {
		from: 'name',
		path: 'key'
	}
});

ProductList.add({
	lang: {type: Types.Select, options: [
		{ value: 'ru', label: 'Русский' },
		{ value: 'en', label: 'English' },
	],
		default: 'ru',
		initial: true,
		label: 'Язык', index: true
	},
	name: {type: String},
	description: {type: Types.Textarea, label: 'Описание' },
	notes: {type: Types.Textarea, label: 'Заметки под таблицей' },
	products: {type: Types.Relationship, ref: 'Product', path: 'products', many: true, label: 'Продукты'}
});


ProductList.defaultColumns = 'name, lang';

ProductList.register();
