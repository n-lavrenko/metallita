const keystone = require('keystone');

let Product = new keystone.List('Product', {
	autokey: {
		from: 'name',
		path: 'key'
	}
});

Product.add({
	name: {type: String},
	diameter: {type: String},
	thickness: {type: String},
	price: {type: String}
});

Product.register();
