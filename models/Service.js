const keystone = require('keystone');
const Types = keystone.Field.Types;

let Service = new keystone.List('Service', {
	autokey: {
		from: 'name',
		path: 'key'
	}
});

Service.add({
	name: {type: String},
	text: {type: Types.Html, wysiwyg: true, label: 'До n см.'},
	image: {
		type: Types.CloudinaryImage,
		autoCleanup: true,
		select : true,
		label: 'Картинка услуги (маленькая, квадратная)'
	}
});

Service.register();
