const keystone = require('keystone');
const Types = keystone.Field.Types;

let Profit = new keystone.List('Profit', {
	autokey: {
		from: 'name',
		path: 'key'
	}
});

Profit.add({
	name: {type: String},
	text: {type: Types.Textarea, height: 200, label: 'Описание'},
});

Profit.register();
