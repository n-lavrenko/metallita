const keystone = require('keystone');
const Types = keystone.Field.Types;


let ProfitList = new keystone.List('ProfitList', {
	autokey: {
		from: 'lang',
		path: 'key'
	}
});

ProfitList.add({
	lang: {type: Types.Select, options: [
		{ value: 'ru', label: 'Русский' },
		{ value: 'en', label: 'English' },
	],
		default: 'ru',
		initial: true,
		label: 'Язык', index: true
	},
	name: {type: String},
	profits: {type: Types.Relationship, ref: 'Profit', path: 'profits', many: true, label: 'Преимущества'}
});


ProfitList.defaultColumns = 'name, lang';

ProfitList.register();
