const keystone = require('keystone');
const Types = keystone.Field.Types;


let StageList = new keystone.List('StageList', {
	autokey: {
		from: 'name',
		path: 'key'
	}
});

StageList.add({
	lang: {type: Types.Select, options: [
		{ value: 'ru', label: 'Русский' },
		{ value: 'en', label: 'English' },
	],
		default: 'ru',
		initial: true,
		label: 'Язык', index: true
	},
	name: {type: String},
	stages: {type: Types.Relationship, ref: 'Stage', path: 'stages', many: true, label: 'Этапы Работы'}
});


StageList.defaultColumns = 'name, lang';

StageList.register();
