const keystone = require('keystone');
const Types = keystone.Field.Types;

let Stage = new keystone.List('Stage', {
	autokey: {
		from: 'name',
		path: 'key'
	}
});

Stage.add({
	name: {type: String},
	text: {
		type: Types.Html,
		wysiwyg: true,
		label: 'Описание'
	},
	image: {
		type: Types.CloudinaryImage,
		autoCleanup: true,
		select : true,
		label: 'Картинка этапа'
	}
});

Stage.register();
