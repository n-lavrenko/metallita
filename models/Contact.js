const keystone = require('keystone');
const Types = keystone.Field.Types;

let Contact = new keystone.List('Contact', {
  autokey: {
    from: 'lang',
    path: 'key'
  },
  label: 'Contacts',
  nocreate: true,
  nodelete: true
});

Contact.add({
	lang: {type: String, noedit: true, label: 'Язык', index: true},
	email: {type: Types.Email, label: 'Основной email'},
	laserEmail: {type: Types.Email, label: 'email лазер. резки'},
	laserPhone: {type: String, label: 'Тел лазер'},
	phoneBY: {type: String, label: 'Тел. гор. РБ'},
	phoneMob: {type: String, label: 'Моб. РБ'},
	phoneRUS: {type: String, label: 'Моб. РФ'},
	addressBY: {type: Types.Html, wysiwyg: true, height: 200, label: 'Адресс РБ'},
	addressRU: {type: Types.Html, wysiwyg: true, height: 200, label: 'Адресс РФ'},

});

Contact.defaultColumns = 'lang, addressBY';

Contact.register();
