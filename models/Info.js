const keystone = require('keystone');
const Types = keystone.Field.Types;

let Info = new keystone.List('Info', {
  autokey: {
    from: 'name',
    path: 'key'
  },
  label: 'Common Info',
  nocreate: true,
  nodelete: true
});

Info.add({
	lang: {type: String, noedit: true, label: 'Язык', index: true},
  meta: {
    title: {type: String},
    description: {type: String},
    keywords: {type: String},
    author: {type: String}
  },
  name: {type: String, label: 'Название компании'},
	url: {type: Types.Url, label: 'URL компании'},
	
	images: {type: Types.CloudinaryImages, autoCleanup: true, select : true, label: 'Картинки слайдера'},
	sliderTexts: { type: Types.TextArray, label: 'Тексты слайдера' },
	
	
	ourServices: {type: String, label: 'Фраза об услугах'},
	image: {
		type: Types.CloudinaryImage,
		autoCleanup: true,
		select : true,
		label: 'Картинка О компании'
	},
	aboutCompany: {type: Types.Html, wysiwyg: true, label: 'О компании' },
	
	yearCount: {type: Number, label: 'Лет работы'},
	orderCount: {type: Number, label: 'Кол-во заказов'},
	professionalCount: {type: Number, label: 'Кол-во профессионалов'},
	
});

Info.defaultColumns = 'name,lang';

Info.register();
