exports.create = {
	Contact: [
		{
			lang: 'ru',
			email: 'info@metallita.com',
			laserEmail: 'laser.metallita@gmail.com',
			laserPhone: '+375296857777',
			phoneBY: '+ 375(17) 233-58-98',
			phoneMob: '+ 375(29) 637-92-36',
			phoneRUS: '+7 (4812) 33-99-60',
			addressBY: 'Минский р-н, аг. Гатово,\n' +
			'Адм. здание инв.№ 600/С-96053 (2 этаж, каб.18)\n' +
			'223017, Минская обл., Республика Беларусь',
			addressRU: '214018, Россия, г.Смоленск, ул.Ново-Киевская, 9'
		},
		{
			lang: 'en',
			email: 'info@metallita.com',
			laserEmail: 'laser.metallita@gmail.com',
			laserPhone: '+375296857777',
			phoneBY: '+ 375(17) 233-58-98',
			phoneMob: '+ 375(29) 637-92-36',
			phoneRUS: '+7 (4812) 33-99-60',
			addressBY: 'Republic of Belarus, Minsk distr., Minsk reg., 223017,\n' +
			'Gatovo, administr.bldg. inv.№ 600/С-96053\n' +
			'(1 floor, off.18)',
			addressRU: ''
		}
	]
};
