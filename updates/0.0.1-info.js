exports.create = {
	Info: [
		{
			lang: 'ru',
			meta: {
				title: 'Металлита - Группа компаний',
				description: 'Металлита - Группа компаний',
				keywords: 'МЕТАЛЛИТА,metallita,Металлита',
				author: 'Металлита ®'
			},
			url: 'metallita.com',
			name: 'Металлита'
		},
		{
			lang: 'en',
			meta: {
				title: 'Metallita - Company Group',
				description: 'Metallita - Company Group',
				keywords: 'Metallita,metallita',
				author: 'Metallita ®'
			},
			url: 'metallita.com',
			name: 'Metallita'
		}
	]
};
