let enName = 'Drill bit tube UNC 1,25';
let enPrice = 'specify a price';

let ruName = 'Корпус коронки UNC 1,25';
let ruPrice = 'уточняйте';


function Product(name, diameter, thickness, price) {
	this.name = name;
	this.diameter = diameter;
	this.thickness = thickness;
	this.price = price;
}

let enProducts = [];
enProducts.push(new Product(enName, 20, 1.5, enPrice));
enProducts.push(new Product(enName, 25, 2.0, enPrice));
enProducts.push(new Product(enName, 28, 1.5, enPrice));
enProducts.push(new Product(enName, 28, 2.0, enPrice));
enProducts.push(new Product(enName, 30, 2.0, enPrice));
enProducts.push(new Product(enName, 34, 2.0, enPrice));
enProducts.push(new Product(enName, 40, 2.0, enPrice));
enProducts.push(new Product(enName, 50, 2.0, enPrice));
enProducts.push(new Product(enName, 60, 2.0, enPrice));
enProducts.push(new Product(enName, 65, 2.0, enPrice));
enProducts.push(new Product(enName, 70, 2.0, enPrice));
enProducts.push(new Product(enName, 80, 2.0, enPrice));
enProducts.push(new Product(enName, 90, 2.0, enPrice));
enProducts.push(new Product(enName, 100, 2.0, enPrice));
enProducts.push(new Product(enName, 110, 2.0, enPrice));
enProducts.push(new Product(enName, 120, 2.0, enPrice));
enProducts.push(new Product(enName, 125, 2.0, enPrice));
enProducts.push(new Product(enName, 130, 2.0, enPrice));
enProducts.push(new Product(enName, 140, 2.0, enPrice));
enProducts.push(new Product(enName, 150, 2.5, enPrice));
enProducts.push(new Product(enName, 160, 2.5, enPrice));
enProducts.push(new Product(enName, 170, 2.5, enPrice));
enProducts.push(new Product(enName, 180, 2.5, enPrice));
enProducts.push(new Product(enName, 190, 2.5, enPrice));
enProducts.push(new Product(enName, 198, 2.5, enPrice));
enProducts.push(new Product(enName, 200, 2.5, enPrice));
enProducts.push(new Product(enName, 223, 2.5, enPrice));
enProducts.push(new Product(enName, 248, 2.5, enPrice));
enProducts.push(new Product(enName, 268, 2.5, enPrice));
enProducts.push(new Product(enName, 298, 2.5, enPrice));
enProducts.push(new Product(enName, 298, 3.0, enPrice));
enProducts.push(new Product(enName, 318, 2.5, enPrice));
enProducts.push(new Product(enName, 323, 2.5, enPrice));
enProducts.push(new Product(enName, 323, 3.0, enPrice));
enProducts.push(new Product(enName, 348, 3.0, enPrice));
enProducts.push(new Product(enName, 398, 2.5, enPrice));
enProducts.push(new Product(enName, 398, 3.0, enPrice));

let ruProducts = [];
enProducts.forEach((el) => {
	ruProducts.push({
		name: ruName,
		diameter: el.diameter,
		thickness: el.thickness,
		price: ruPrice
	});
});

let allProducts = enProducts.concat(ruProducts);


exports.create = {
	Product: allProducts
};
